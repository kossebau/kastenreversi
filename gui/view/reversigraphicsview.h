/*******************************************************************
 *
 * Copyright 2006 Dmitry Suzdalev <dimsuz@gmail.com>
 *
 * This file is part of the KDE project "KReversi"
 *
 * KReversi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * KReversi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KReversi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 ********************************************************************/
#ifndef REVERSI_GRAPHICSVIEW_H
#define REVERSI_GRAPHICSVIEW_H

// Qt Gui
#include <QGraphicsView>


namespace Kasten2
{
class ReversiScene;

class ReversiGraphicsView : public QGraphicsView
{
public:
    ReversiGraphicsView( ReversiScene* scene, QWidget* parent = 0 );

protected: // QGraphicsView API
    virtual void resizeEvent( QResizeEvent* event );

private:
    ReversiScene* m_scene;
};

}

#endif
