/*
    This file is part of the Reversi Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "reversiview.h"

// module
#include "reversigraphicsview.h"
#include "reversiscene.h"
// core module
#include <reversidocument.h>


namespace Kasten2
{

ReversiView::ReversiView( ReversiDocument* document )
  : AbstractView( document ),
    mDocument( document )
{
    mScene = new ReversiScene( mDocument, QLatin1String( "chip_color" ) );
    mGraphicsView = new ReversiGraphicsView( mScene );

    // propagate signals
    connect( mDocument, SIGNAL(titleChanged( QString )), SIGNAL(titleChanged( QString )) );
//     connect( mGraphicsView, SIGNAL(selectionChanged( bool )), SIGNAL(hasSelectedDataChanged( bool )) );
}

const AbstractModelSelection* ReversiView::modelSelection() const { return 0;}//&mSelection; }

bool ReversiView::hasFocus() const { return mGraphicsView->focusWidget()->hasFocus(); }

QWidget* ReversiView::widget()             const { return mGraphicsView; }
QString ReversiView::title()               const { return mDocument->title(); }
bool ReversiView::isModifiable()           const { return true; }
bool ReversiView::isReadOnly()             const { return true; }//mGraphicsView->isReadOnly(); }

void ReversiView::setFocus() { mGraphicsView->setFocus(); }

void ReversiView::setReadOnly( bool isReadOnly ) { }//mGraphicsView->setReadOnly( isReadOnly ); }

void ReversiView::selectAllData( bool selectAll )
{
Q_UNUSED( selectAll )
//     mGraphicsView->selectAll( selectAll );
}

bool ReversiView::hasSelectedData() const
{
    return false;//mGraphicsView->hasSelectedData();
}

QMimeData* ReversiView::copySelectedData() const
{
    return 0; //mGraphicsView->selectionAsMimeData();
}


#if 0
void ReversiView::onSelectionChange( bool selected )
{
    mSelection.setSection( mGraphicsView->selection() );
    emit hasSelectedDataChanged( selected );
}
#endif

void ReversiView::hint()
{
    mScene->slotHint();
}

bool ReversiView::demoModus() const
{
    return mScene->isInDemoMode();
}

void ReversiView::setDemoModus( bool modus )
{
    if( mScene->isInDemoMode() == modus )
        return;

    mScene->toggleDemoMode( modus );
    emit demoModusChanged( modus );
}

bool ReversiView::showsLastMove() const
{
    return mScene->showsLastMove();
}

bool ReversiView::showsLegalMoves() const
{
    return mScene->showsLegalMoves();
}

bool ReversiView::usesColoredChips() const
{
    return (mScene->chipsPrefix() == QLatin1String("chip_color"));
}

int ReversiView::animationSpeed() const
{
    return mScene->animationSpeed();
}


void ReversiView::setShowsLastMove( bool shows )
{
    mScene->setShowLastMove( shows );
}

void ReversiView::setShowsLegalMoves( bool shows )
{
    mScene->setShowLegalMoves( shows );
}

void ReversiView::setUsesColoredChips( bool uses )
{
    mScene->setChipsPrefix( QLatin1String(uses ? "chip_color" : "chip_bw") );
}

void ReversiView::setAnimationSpeed( int speed )
{
    mScene->setAnimationSpeed( speed );
}


ReversiView::~ReversiView()
{
    delete mGraphicsView;
}

}
