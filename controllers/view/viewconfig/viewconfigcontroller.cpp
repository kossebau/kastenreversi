/*
    This file is part of the Reversi Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "viewconfigcontroller.h"

// lib
#include <reversiview.h>
// KDE
#include <KXMLGUIClient>
#include <KLocale>
#include <KActionCollection>
#include <KToggleAction>
#include <KSelectAction>


namespace Kasten2
{

ViewConfigController::ViewConfigController( KXMLGUIClient* guiClient )
    : AbstractXmlGuiController()
    , mReversiView( 0 )
{
    KActionCollection* const actionCollection = guiClient->actionCollection();

    mShowLastMoveAction =
        actionCollection->add<KToggleAction>( QLatin1String("show_last_move") );
    mShowLastMoveAction->setIcon( KIcon(QLatin1String("lastmoves")) );
    mShowLastMoveAction->setText( i18nc("@option:check",
                                        "Show Last Move") );
    connect( mShowLastMoveAction, SIGNAL(triggered(bool)),
             SLOT(toggleLastMove(bool)) );

    mShowValidMovesAction =
        actionCollection->add<KToggleAction>( QLatin1String("show_legal_moves") );
    mShowValidMovesAction->setIcon( KIcon(QLatin1String("legalmoves")) );
    mShowValidMovesAction->setText( i18nc("@option:check",
                                          "Show Legal Moves") );
    connect( mShowValidMovesAction, SIGNAL(triggered(bool)),
             SLOT(toggleValidMoves(bool)) );

    mUseColoredChipsAction =
        actionCollection->add<KToggleAction>( QLatin1String("use_colored_chips") );
    mUseColoredChipsAction->setText( i18nc("@option:check",
                                           "Use Colored Chips") );
    connect( mUseColoredChipsAction, SIGNAL(triggered(bool)),
             SLOT(toggleColoredChips(bool)) );

    mAnimationSpeedAction =
        actionCollection->add<KSelectAction>( QLatin1String("anim_speed") );
    mAnimationSpeedAction->setText( i18nc("@title:menu",
                                          "Animation Speed") );
    QStringList list;
    list.append( i18nc("@item:inmenu encoding of the bytes as values in the hexadecimal format",
                       "&Slow") );
    list.append( i18nc("@item:inmenu encoding of the bytes as values in the decimal format",
                       "&Normal") );
    list.append( i18nc("@item:inmenu encoding of the bytes as values in the octal format",
                       "&Fast") );
    mAnimationSpeedAction->setItems( list );
    connect( mAnimationSpeedAction, SIGNAL(triggered(int)), SLOT(setAnimationSpeed(int)) );

    setTargetModel( 0 );
}

void ViewConfigController::setTargetModel( AbstractModel* model )
{
    mReversiView = model ? model->findBaseModel<ReversiView*>() : 0;

    const bool hasView = ( mReversiView != 0 );
    mShowLastMoveAction->setChecked( hasView ? mReversiView->showsLastMove() : false );
    mShowValidMovesAction->setChecked( hasView ? mReversiView->showsLegalMoves() : false );
    mUseColoredChipsAction->setChecked( hasView ? mReversiView->usesColoredChips() : false );
    mAnimationSpeedAction->setCurrentItem( hasView ? mReversiView->animationSpeed() : 0 );
    mShowLastMoveAction->setEnabled( hasView );
    mShowValidMovesAction->setEnabled( hasView );
    mUseColoredChipsAction->setEnabled( hasView );
    mAnimationSpeedAction->setEnabled( hasView );
}


void ViewConfigController::toggleLastMove( bool shows )
{
    mReversiView->setShowsLastMove( shows );
}

void ViewConfigController::toggleValidMoves( bool shows )
{
    mReversiView->setShowsLegalMoves( shows );
}

void ViewConfigController::toggleColoredChips(bool uses)
{
    mReversiView->setUsesColoredChips( uses );
}

void ViewConfigController::setAnimationSpeed( int speed )
{
    mReversiView->setAnimationSpeed( speed );
}

}
