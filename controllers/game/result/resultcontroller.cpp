/*
    This file is part of the Reversi Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "resultcontroller.h"

// lib
#include <reversidocument.h>
// KDE
#include <KMessageBox>
#include <KLocale>

#include <KDebug>

namespace Kasten2
{

ResultController::ResultController( QWidget* mainWindow )
  : AbstractXmlGuiController()
  , mMainWindow( mainWindow )
  , mReversiDocument( 0 )
{
    setTargetModel( 0 );
}


void ResultController::setTargetModel( AbstractModel* model )
{
    if( mReversiDocument ) mReversiDocument->disconnect( this );

    mReversiDocument = model ? model->findBaseModel<ReversiDocument*>() : 0;

    const bool hasView = ( mReversiDocument != 0 );
    if( hasView )
    {
        connect( mReversiDocument, SIGNAL(gameTurnStatusChanged(int)),
                 SLOT(onGameTurnStatusChanged(int)) );
    }
}


void ResultController::onGameTurnStatusChanged( int gameTurnStatus )
{
    if( gameTurnStatus != GameOver )
        return;

    int player1Score = mReversiDocument->playerScore( Black );
    int player2Score = mReversiDocument->playerScore( White );

    QString result =
        ( player1Score == player2Score ) ? i18n("Game is drawn!") :
        ( player1Score > player2Score ) ?  i18n("You win!") :
        /* else */                         i18n("You have lost!");

    result += i18n( "<br />You: %1", player1Score );
    result += i18n( "<br />Computer: %1", player2Score );

    KMessageBox::information( mMainWindow, result, i18n("Game over") );
}

}
