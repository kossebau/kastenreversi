/*
    This file is part of the Reversi Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RESULTCONTROLLER_H
#define RESULTCONTROLLER_H

// lib
#include "reversikastencontrollers_export.h"
// Kasten gui
#include <Kasten2/AbstractXmlGuiController>

class QWidget;


namespace Kasten2
{
class ReversiDocument;


class REVERSIKASTENCONTROLLERS_EXPORT ResultController : public AbstractXmlGuiController
{
  Q_OBJECT

  public:
    explicit ResultController( QWidget* mainWindow );

  public: // AbstractXmlGuiController API
    virtual void setTargetModel( AbstractModel* model );

  protected Q_SLOTS:
    void onGameTurnStatusChanged( int gameTurnStatus );

  protected:
    QWidget* mMainWindow;

    ReversiDocument* mReversiDocument;
};

}

#endif
