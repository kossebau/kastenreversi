/*
    This file is part of the Reversi Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "scoresbarcontroller.h"

// lib
#include <reversidocument.h>
// Kasten ui
#include <Kasten2/StatusBar>
// KDE
#include <KLocale>
// Qt
#include <QtGui/QLabel>
#include <QtGui/QFontMetrics>

#include <KDebug>

namespace Kasten2
{

ScoresBarController::ScoresBarController( StatusBar* statusBar )
  : AbstractXmlGuiController()
  , mStatusBar( statusBar )
  , mReversiDocument( 0 )
{
    mPlayer1ScoresLabel = new QLabel( statusBar );
    statusBar->addWidget( mPlayer1ScoresLabel );

    mPlayer2ScoresLabel = new QLabel( statusBar );
    statusBar->addWidget( mPlayer2ScoresLabel );

    fixWidths();

    setTargetModel( 0 );
}


static QString myScoreLabel( int score )
{
    const QString scoreString = (score < 0) ?
            i18nc("@info:status turn status not available",
                  "-") :
            QString::number( score );

    return i18n( "You: %1", scoreString );
}

static QString opponentScoreLabel( int score )
{
    const QString scoreString = (score < 0) ?
            i18nc("@info:status turn status not available",
                  "-") :
            QString::number( score );

    return i18n( "Computer: %1", scoreString );
}

void ScoresBarController::fixWidths()
{
    const QFontMetrics metrics = mStatusBar->fontMetrics();

    int largestPlayer1ScoresWidth = 0;
    int largestPlayer2ScoresWidth = 0;
    for( int digit = 1; digit <= 9; ++digit )
    {
        const int score = digit*10 + digit;

        const int player1ScoresWidth = metrics.boundingRect( myScoreLabel(score) ).width();
        if( largestPlayer1ScoresWidth < player1ScoresWidth )
            largestPlayer1ScoresWidth = player1ScoresWidth;

        const int player2ScoresWidth = metrics.boundingRect( opponentScoreLabel(score) ).width();
        if( largestPlayer2ScoresWidth < player2ScoresWidth )
            largestPlayer2ScoresWidth = player2ScoresWidth;
    }

    mPlayer1ScoresLabel->setFixedWidth( largestPlayer1ScoresWidth );
    mPlayer2ScoresLabel->setFixedWidth( largestPlayer2ScoresWidth );
}

void ScoresBarController::setTargetModel( AbstractModel* model )
{
    if( mReversiDocument ) mReversiDocument->disconnect( this );

    mReversiDocument = model ? model->findBaseModel<ReversiDocument*>() : 0;

    const bool hasView = ( mReversiDocument != 0 );
    if( hasView )
    {
        connect( mReversiDocument, SIGNAL(scoresChanged(int,int)),
                 SLOT(onScoresChanged(int,int)) );

        onScoresChanged( mReversiDocument->playerScore(Black),
                         mReversiDocument->playerScore(White) );
    }
    else
        onScoresChanged( -1, -1 );

    mPlayer1ScoresLabel->setEnabled( hasView );
    mPlayer2ScoresLabel->setEnabled( hasView );
}


void ScoresBarController::onScoresChanged( int player1Score, int player2Score )
{
    mPlayer1ScoresLabel->setText( myScoreLabel(player1Score) );
    mPlayer2ScoresLabel->setText( opponentScoreLabel(player2Score) );
}

}
