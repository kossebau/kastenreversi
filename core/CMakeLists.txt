project(reversikastencore)

include_directories(
  document
  ${KASTEN2SPIELE0_INCLUDE_DIRS}
  ${KASTEN2_INCLUDE_DIRS}
)

set( reversikasten_document_SRCS
  document/reversidocumentfactory.cpp
  document/reversidocument.cpp
  document/reversiengine.cpp
)

set( reversikasten_io_filesystem_SRCS
  io/filesystem/reversiqvariantcodec.cpp
  io/filesystem/reversifilewritethread.cpp
  io/filesystem/reversifileloadthread.cpp
  io/filesystem/reversifilereloadthread.cpp
  io/filesystem/reversifileconnectjob.cpp
  io/filesystem/reversifilewritejob.cpp
  io/filesystem/reversifilewritetojob.cpp
  io/filesystem/reversifileloadjob.cpp
  io/filesystem/reversifilereloadjob.cpp
  io/filesystem/reversifilesynchronizer.cpp
  io/filesystem/reversifilesynchronizerfactory.cpp
)

set( reversikasten_io_SRCS
  ${reversikasten_io_filesystem_SRCS}
)
set( reversikasten_core_LIB_SRCS
  ${reversikasten_document_SRCS}
  ${reversikasten_io_SRCS}
)

kde4_add_library( ${reversikasten_core_LIB}  SHARED ${reversikasten_core_LIB_SRCS} )

target_link_libraries( ${reversikasten_core_LIB}
  ${KASTEN2SPIELE0_CORE_LIB}
  ${KASTEN2_CORE_LIB}
  ${KDE4_KDECORE_LIBS}
  ${QJSON_LIBRARIES}
)
set_target_properties( ${reversikasten_core_LIB}  PROPERTIES
  VERSION   ${REVERSIKASTEN_LIB_VERSION}
  SOVERSION ${REVERSIKASTEN_LIB_SOVERSION}
)

install( TARGETS ${reversikasten_core_LIB}  ${INSTALL_TARGETS_DEFAULT_ARGS} )
