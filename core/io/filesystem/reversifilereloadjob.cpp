/*
    This file is part of the Kasten Reversi program, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "reversifilereloadjob.h"

// lib
// #include "externalbookmarkstorage.h"
#include "reversifilesynchronizer.h"
#include "reversifilereloadthread.h"
#include <reversidocument.h>
// KDE
#include <KUrl>
// Qt
#include <QtCore/QCoreApplication>


namespace Kasten2
{

ReversiFileReloadJob::ReversiFileReloadJob( ReversiFileSynchronizer* synchronizer )
 : AbstractFileSystemSyncFromRemoteJob( synchronizer )
{}

void ReversiFileReloadJob::startReadFromFile()
{
    ReversiDocument* reversiDocument = qobject_cast<ReversiDocument*>( synchronizer()->document() );
    ReversiFileReloadThread* reloadThread = new ReversiFileReloadThread( this, /*document, */file() );
    reloadThread->start();
    while( !reloadThread->wait(100) )
        QCoreApplication::processEvents( QEventLoop::ExcludeUserInputEvents | QEventLoop::ExcludeSocketNotifiers, 100 );

    bool success = reloadThread->success();
    // TODO: moved this here to avoid marshalling the change signals out of the thread. Good idea?
    if( success )
    {
//         reversiDocument->setCounter( reloadThread->counter() );
    }
    else
    {
        setError( KJob::KilledJobError );
        setErrorText( reloadThread->errorString() );
    }

    delete reloadThread;

    completeRead( success );
}

ReversiFileReloadJob::~ReversiFileReloadJob() {}

}
