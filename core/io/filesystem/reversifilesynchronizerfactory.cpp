/*
    This file is part of the Kasten Reversi program, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "reversifilesynchronizerfactory.h"

// lib
#include "reversifilesynchronizer.h"
#include <reversidocument.h>
// KDE
#include <KUrl>
// Qt
#include <QtCore/QLatin1String>


namespace Kasten2
{

ReversiFileSynchronizerFactory::ReversiFileSynchronizerFactory() {}

// could be set to base class as value, is only one object per factory at runtime
// and settable in desktop file
QString ReversiFileSynchronizerFactory::supportedWorkType() const { return QLatin1String("ReversiDocument");}
QString ReversiFileSynchronizerFactory::supportedRemoteType() const { return QLatin1String("application/x-reversi.json");}


AbstractModelSynchronizer* ReversiFileSynchronizerFactory::createSynchronizer() const
{
    return new ReversiFileSynchronizer();
}

ReversiFileSynchronizerFactory::~ReversiFileSynchronizerFactory() {}

}
