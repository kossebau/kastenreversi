/*
    This file is part of the Kasten Reversi program, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "reversifilesynchronizer.h"

// lib
#include "reversifileloadjob.h"
#include "reversifileconnectjob.h"
#include "reversifilewritejob.h"
#include "reversifilereloadjob.h"
#include "reversifilewritetojob.h"
#include <reversidocument.h>
// KDE
#include <KUrl>


namespace Kasten2
{

ReversiFileSynchronizer::ReversiFileSynchronizer()
  : mDocument( 0 )
{
    connect( this, SIGNAL(urlChanged(KUrl)), SLOT(onUrlChange(KUrl)) );
}

AbstractDocument* ReversiFileSynchronizer::document() const { return mDocument; }
LocalSyncState ReversiFileSynchronizer::localSyncState() const
{
    return mDocument ?
        (mDocument->isModified() ? LocalHasChanges : LocalInSync) : LocalInSync;
}

void ReversiFileSynchronizer::setDocument( ReversiDocument* document )
{
    mDocument = document;
    if( mDocument )
        connect( mDocument, SIGNAL(modificationChanged(bool)),
                 SLOT(onModificationChanged(bool)) );
}

void ReversiFileSynchronizer::startOffering( AbstractDocument* document ) { Q_UNUSED(document) }

AbstractLoadJob* ReversiFileSynchronizer::startLoad( const KUrl& url )
{
    return new ReversiFileLoadJob( this, url );
}

AbstractSyncToRemoteJob* ReversiFileSynchronizer::startSyncToRemote()
{
    return new ReversiFileWriteJob( this );
}

AbstractSyncFromRemoteJob* ReversiFileSynchronizer::startSyncFromRemote()
{
    return new ReversiFileReloadJob( this );
}

AbstractSyncWithRemoteJob* ReversiFileSynchronizer::startSyncWithRemote( const KUrl& url, AbstractModelSynchronizer::ConnectOption option  )
{
    return new ReversiFileWriteToJob( this, url, option );
}

AbstractConnectJob* ReversiFileSynchronizer::startConnect( AbstractDocument* document,
                                                           const KUrl& url,
                                                           AbstractModelSynchronizer::ConnectOption option )
{
    return new ReversiFileConnectJob( this, document, url, option );
}

void ReversiFileSynchronizer::onUrlChange( const KUrl& url )
{
//     mDocument->setTitle( url.fileName() ); TODO: add setTitle
}

void ReversiFileSynchronizer::onModificationChanged( bool isModified )
{
    emit localSyncStateChanged( (isModified ? LocalHasChanges : LocalInSync) );
}

}
