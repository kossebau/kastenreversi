/*
    This file is part of the Kasten Reversi program, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "reversifileloadthread.h"

// lib
#include "reversiqvariantcodec.h"
#include <reversidocument.h>
// KDE
#include <KLocale>
// QJSon
#include <qjson/parser.h>
// Qt
#include <QtCore/QCoreApplication>
#include <QtCore/QDataStream>
#include <QtCore/QFile>
// C++
#include <limits>


namespace Kasten2
{

void ReversiFileLoadThread::run()
{
    const qint64 fileSize = mFile->size();

    // check if the file content can be addressed with int
    const qint32 maxInt32 = std::numeric_limits<qint32>::max();
    const int maxInt = std::numeric_limits<int>::max();

    bool success = ( maxInt > maxInt32 || fileSize <= maxInt32 );

    if( success )
    {
        // allocate working memory
        QByteArray storedData;
        storedData.resize( fileSize );
        bool success = ( storedData.size() == fileSize );

        if( success )
        {
            QDataStream inStream( mFile );
            inStream.readRawData( storedData.data(), fileSize );

            success = ( inStream.status() == QDataStream::Ok );

            if( success )
            {
                QJson::Parser jsonParser;

                const QVariant parsedData = jsonParser.parse( storedData, &success ).toMap();

                if( success )
                {
                    mDocument = ReversiQVariantCodec::toReversiDocument( parsedData );
                    mDocument->moveToThread( QCoreApplication::instance()->thread() );
                }
                else
                    mErrorString = i18n( "Format of file seems broken: %1", jsonParser.errorString() );
            }
            else
                mErrorString = mFile->errorString();
        }
        else
            mErrorString = i18n( "There is not enough free working memory to load this file." );
    }
    else
        mErrorString = i18n( "This file seems to be too large." );

    if( ! success )
        mDocument = 0;

    emit documentRead( mDocument );
}

ReversiFileLoadThread::~ReversiFileLoadThread() {}

}
