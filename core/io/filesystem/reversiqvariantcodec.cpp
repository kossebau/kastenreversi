/*
    This file is part of the Kasten Reversi program, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "reversiqvariantcodec.h"

// lib
#include <reversidocument.h>
// Qt Core
#include <QtCore/QVariant>


namespace Kasten2
{

static const char movesId[] =   "Moves";
static const char partyId[] =   "Party";
static const char rowId[] =     "Row";
static const char columndId[] = "Column";
static const char changedId[] = "Changed";
static const char boardId[] =   "Board";


static
QVariantList
toQVariantList( const QStack<PosList>& moveStack )
{
    const QString partyKey =   QLatin1String( partyId );
    const QString rowKey =     QLatin1String( rowId );
    const QString columnKey =  QLatin1String( columndId );
    const QString changedKey = QLatin1String( changedId );

    QVariantList result;

    foreach( PosList posList, moveStack )
    {
        QVariantMap move;

        const KReversiPos pos = posList.takeFirst();
        move.insert( partyKey,  pos.color );
        move.insert( rowKey,    pos.row );
        move.insert( columnKey, pos.col );

        QVariantList changes;
        foreach( const KReversiPos& pos, posList)
        {
            QVariantList change;
            change << pos.color << pos.row << pos.col;

            changes << QVariant(change);
        }
        move.insert( changedKey, changes );

        result << move;
    }

    return result;
}

static
QStack<PosList>
toPosListStack( const QVariantList& moves )
{
    const QString partyKey =   QLatin1String( partyId );
    const QString rowKey =     QLatin1String( rowId );
    const QString columnKey =  QLatin1String( columndId );
    const QString changedKey = QLatin1String( changedId );

    QStack<PosList> result;

    foreach( const QVariant& m, moves )
    {
        const QVariantMap move = m.toMap();

        const int party =  move.value( partyKey ).toInt();
        const int row =    move.value( rowKey ).toInt();
        const int column = move.value( columnKey ).toInt();

        PosList posList;
        posList.append( KReversiPos((ChipColor)party, row, column) );

        const QVariantList changes = move.value( changedKey ).toList();
        foreach( const QVariant& c, changes )
        {
            const QVariantList change = c.toList();
            if( change.count() != 3 )
                return QStack<PosList>(); // should not happen

            const int party = change.at(0).toInt();
            const int row = change.at(1).toInt();
            const int column = change.at(2).toInt();

            posList.append( KReversiPos((ChipColor)party, row, column) );
        }

        result.append( posList );
    }

    return result;
}

static
QVariantList
toQVariantList( const ReversiBoard& board )
{
    QVariantList result;

    for( int r = 0; r < 8; ++r )
    {
        QVariantList row;
        for( int c = 0; c < 8; ++c )
            row << board.position( r, c );

        result << QVariant( row );
    }

    return result;
}

static
ReversiBoard
toReversiBoard( const QVariantList& board )
{
    ReversiBoard result;

    for( int r = 0; r < 8; ++r )
    {
        const QVariantList row = board.at(r).toList();
        for( int c = 0; c < 8; ++c )
            result.setPosition( r, c, (ChipColor)row.at(c).toInt() );
    }

    return result;
}


QVariant ReversiQVariantCodec::toQVariant( const ReversiDocument& document )
{
    QVariantMap data;

    const QVariantList moves = toQVariantList( document.undoStack() );
    data.insert( QLatin1String(movesId), moves );

    const QVariantList board = toQVariantList( document.board() );
    data.insert( QLatin1String(boardId), board );

    return data;
}


ReversiDocument* ReversiQVariantCodec::toReversiDocument( const QVariant& data )
{
    ReversiDocument* result = 0;

    const QVariantMap map = data.toMap();

    const QVariantList moves = map.value( QLatin1String(movesId) ).toList();
    const QStack<PosList> undoStack = toPosListStack( moves );

    const QVariantList boardAsQVariant = map.value( QLatin1String(boardId) ).toList();
    const ReversiBoard board = toReversiBoard( boardAsQVariant );

    result = new ReversiDocument();
    result->reset( undoStack, board );

    return result;
}

bool ReversiQVariantCodec::toReversiDocument( ReversiDocument* document, const QVariant& data )
{
    if( ! document )
        return false;

    const QVariantMap map = data.toMap();

    const QVariantList moves = map.value( QLatin1String(movesId) ).toList();
    const QStack<PosList> undoStack = toPosListStack( moves );

    const QVariantList boardAsQVariant = map.value( QLatin1String(boardId) ).toList();
    const ReversiBoard board = toReversiBoard( boardAsQVariant );

    document->reset( undoStack, board );

    return true;
}

}
