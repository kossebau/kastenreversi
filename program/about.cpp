/*
    This file is part of the Kasten Reversi program, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy 
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "about.h"

// KDE
#include <KLocale>


// Program
static const char ProgramId[] =          "kastenreversi";
static const char ProgramVersion[] =     "3.0.0";
static const char ProgramHomepage[] =    "http://games.kde.org/kreversi";


ReversiAboutData::ReversiAboutData()
: KAboutData( ProgramId, 0,
              ki18n("Kasten Reversi"), ProgramVersion, // name
              ki18n("Reversi Board Game, built on Kasten"), // description
              KAboutData::License_GPL_V2,
              ki18n("Copyright 1997-2000 Mario Weilguni\n"
                    "Copyright 2004-2006 Inge Wallin\n"
                    "Copyright 2006 Dmitry Suzdalev\n"
                    "Copyright 2011 Friedrich W. H. Kossebau"), //copyright
              ki18n("Play!"), // comment
              ProgramHomepage )
{
    setOrganizationDomain( "kde.org" );
    setProgramIconName( QLatin1String("kreversi") );
    addAuthor(ki18n("Mario Weilguni"),ki18n("Original author"), "mweilguni@sime.com");
    addAuthor(ki18n("Inge Wallin"),ki18n("Original author"), "inge@lysator.liu.se");
    addAuthor(ki18n("Dmitry Suzdalev"), ki18n("Game rewrite for KDE4. Current maintainer."), "dimsuz@gmail.com");
    addAuthor( ki18n("Friedrich W. H. Kossebau"),ki18n("Port to Kasten"), "kossebau@kde.org" );
    addCredit(ki18n("Simon Hürlimann"), ki18n("Action refactoring"));
    addCredit(ki18n("Mats Luthman"), ki18n("Game engine, ported from his JAVA applet."));
    addCredit(ki18n("Arne Klaassen"), ki18n("Original raytraced chips."));
    addCredit(ki18n("Mauricio Piacentini"), ki18n("Vector chips and background for KDE4."));
    addCredit(ki18n("Brian Croom"), ki18n("Port rendering code to KGameRenderer"), "brian.s.croom@gmail.com");
}
