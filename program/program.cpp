/*
    This file is part of the Kasten Reversi program, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy 
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "program.h"

// program
#include "mainwindow.h"
#include "highscoremanager.h"
// Reversi Kasten
#include <reversiviewfactory.h>
#include <reversidocumentfactory.h>
#include <filesystem/reversifilesynchronizerfactory.h>
// Kasten gui
#include <Kasten2/SingleDocumentStrategy>
#include <Kasten2/DialogHandler>
#include <Kasten2/ViewManager>
// Kasten core
#include <Kasten2/DocumentManager>
#include <Kasten2/DocumentCreateManager>
#include <Kasten2/DocumentSyncManager>
// KDE
#include <KLocale>
#include <KUrl>
#include <KCmdLineArgs>
#include <KApplication>
// Qt
#include <QtCore/QList>


namespace Kasten2
{

static const char createOptionId[] = "create";
static const char createOptionShortId[] = "c";
static const char generatorOptionWithTemplateId[] = "generator <name>";
static const char generatorOptionId[] = "generator";
static const char generatorOptionShortId[] = "g";
static const char demoOptionId[] = "demo";
static const char demoOptionShortId[] = "d";

#define CmdLineOptionName(STRING) QByteArray::fromRawData( STRING, sizeof(STRING)-1 )

ReversiProgram::ReversiProgram( int argc, char* argv[] )
  : mDocumentManager( new DocumentManager() )
  , mViewManager( new ViewManager() )
  , mDocumentStrategy( new SingleDocumentStrategy(mDocumentManager, mViewManager) )
  , mDialogHandler( new DialogHandler() )
{
    KCmdLineOptions programOptions;
    programOptions.add( CmdLineOptionName(createOptionShortId) )
                  .add( CmdLineOptionName(createOptionId),
                        ki18n("Create a new Reversi Game") );
    programOptions.add( CmdLineOptionName(generatorOptionShortId) )
                  .add( CmdLineOptionName(generatorOptionWithTemplateId),
                        ki18n("Define the generator for creating the new Reversi Game"), QByteArray() );
    programOptions.add( CmdLineOptionName(demoOptionShortId) )
                  .add( CmdLineOptionName(demoOptionId),
                        ki18n("Start with demo game playing") );
    programOptions.add( CmdLineOptionName("+[URL]"), ki18n("Game file to load") );

    KCmdLineArgs::init( argc, argv, &mAboutData );
    KCmdLineArgs::addCmdLineOptions( programOptions );
}


int ReversiProgram::execute()
{
    KApplication programCore;

    HighscoreManager highscoreManager;

    mDocumentManager->createManager()->setDocumentFactory( new ReversiDocumentFactory() );
    mDocumentManager->syncManager()->setDocumentSynchronizerFactory( new ReversiFileSynchronizerFactory() );
    mDocumentManager->syncManager()->setOverwriteDialog( mDialogHandler );
    mDocumentManager->syncManager()->setSaveDiscardDialog( mDialogHandler );
    mViewManager->setViewFactory( new ReversiViewFactory() );

    ReversiMainWindow* mainWindow = new ReversiMainWindow( this );
    mDialogHandler->setWidget( mainWindow );

    // started by session management?
    if( programCore.isSessionRestored() && KMainWindow::canBeRestored(1) )
    {
        mainWindow->restore( 1 );
    }
    else
    {
        // no session.. just start up normally
        KCmdLineArgs* arguments = KCmdLineArgs::parsedArgs();

        // take arguments
        if( arguments->isSet(CmdLineOptionName( createOptionId )) )
        {
            const QString generatorId =
                arguments->getOption( CmdLineOptionName(generatorOptionId) );
            if( generatorId.isEmpty() )
                mDocumentStrategy->createNew();
        }
        else if( arguments->count() > 0 )
        {
            mDocumentStrategy->load( arguments->url(0) );
        }

        mainWindow->show();

//         if( arguments->isSet(CmdLineOptionName( demoOptionId )) )
        {
        }

        arguments->clear();
    }

    return programCore.exec();
}


void ReversiProgram::quit()
{
    kapp->quit();
}


ReversiProgram::~ReversiProgram()
{
    delete mDocumentStrategy;
    delete mDocumentManager;
    delete mViewManager;
    delete mDialogHandler;
}

}
