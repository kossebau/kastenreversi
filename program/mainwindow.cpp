/*
    This file is part of the Kasten Reversi program, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy 
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"

// program
#include "program.h"
#include <viewconfig/viewconfigcontroller.h>
#include <scores/scoresbarcontroller.h>
#include <result/resultcontroller.h>
#include <reversiview.h>
// Spiele Kasten controllers
#include <Kasten2/Spiele0/GameCreatorController>
#include <Kasten2/Spiele0/GameLoaderController>
#include <Kasten2/Spiele0/GameSetRemoteController>
#include <Kasten2/Spiele0/GameSynchronizeController>
#include <Kasten2/Spiele0/GameQuitController>
#include <Kasten2/Spiele0/HighscoresController>
#include <Kasten2/Spiele0/DemoController>
#include <Kasten2/Spiele0/HintController>
#include <Kasten2/Spiele0/GameTurnStatusBarController>
#include <Kasten2/Spiele0/DifficultyController>
#include <Kasten2/Spiele0/DifficultyBarController>
// Kasten controllers
#include <Kasten2/FullScreenController>
// Kasten gui
#include <Kasten2/SingleDocumentStrategy>
#include <Kasten2/ViewManager>
#include <Kasten2/StatusBar>
// Kasten core
#include <Kasten2/DocumentCreateManager>
#include <Kasten2/DocumentSyncManager>
#include <Kasten2/DocumentManager>
// KDE
#include <KGlobal>
#include <KConfigGroup>


namespace Kasten2
{

static const char LoadedUrlsKey[] = "LoadedUrls";

// TODO: make this a single window, without
// also make a old-fashioned version, which preloads new created game instance
// and stays alive if played game is closed or new instance is requested
ReversiMainWindow::ReversiMainWindow( ReversiProgram* program )
  : SingleViewWindow( 0 )
  , mProgram( program )
{
    setObjectName( QLatin1String("Shell") );

    ViewManager* const viewManager = program->viewManager();
    connect( viewManager, SIGNAL(opened(QList<Kasten2::AbstractView*>)),
             SLOT(onViewsOpened(QList<Kasten2::AbstractView*>)) );
    connect( viewManager, SIGNAL(closing(QList<Kasten2::AbstractView*>)),
             SLOT(onViewsClosing(QList<Kasten2::AbstractView*>)) );
//     connect( viewArea(), SIGNAL(dataOffered(const QMimeData*,bool&)),
//              SLOT(onDataOffered(const QMimeData*,bool&)) );
//     connect( viewArea(), SIGNAL(dataDropped(const QMimeData*)),
//              SLOT(onDataDropped(const QMimeData*)) );

    setStatusBar( new Kasten2::StatusBar(this) );

    setupControllers();
    setupGUI();
}

void ReversiMainWindow::setupControllers()
{
    SingleDocumentStrategy* const documentStrategy = mProgram->documentStrategy();
    DocumentManager* const documentManager = mProgram->documentManager();
    DocumentSyncManager* const syncManager = documentManager->syncManager();

    // part of Reversi Kasten
    addXmlGuiController( new ResultController(this) );
    addXmlGuiController( new ViewConfigController(this) );
    // part of Spiele Kasten
    addXmlGuiController( new GameCreatorController(documentStrategy, this) );
    addXmlGuiController( new GameLoaderController(documentStrategy, this) );
    addXmlGuiController( new GameSynchronizeController(syncManager, this) );
    addXmlGuiController( new GameSetRemoteController(syncManager, this) );
    addXmlGuiController( new HighscoresController(this, this) );
    addXmlGuiController( new DemoController(this) );
    addXmlGuiController( new HintController(this) );
    addXmlGuiController( new DifficultyController(this) );
    // general, part of Kasten
    addXmlGuiController( new FullScreenController(this) );
    addXmlGuiController( new GameQuitController(this) );

    Kasten2::StatusBar* const bottomBar = static_cast<Kasten2::StatusBar*>( statusBar() );
    addXmlGuiController( new GameTurnStatusBarController(bottomBar) );
    addXmlGuiController( new ScoresBarController(bottomBar) );
    addXmlGuiController( new DifficultyBarController(bottomBar) );
}

bool ReversiMainWindow::queryClose()
{
    AbstractView* view = this->view();
    AbstractDocument* document = view ? view->findBaseModel<AbstractDocument*>() : 0;
    // TODO: query the document manager or query the view manager?
    return (! document) || mProgram->documentManager()->canClose( document );
}

void ReversiMainWindow::saveProperties( KConfigGroup& configGroup )
{
//     const QStringList urls = mDocumentManager->urls();
//     configGroup.writePathEntry( LoadedUrlsKey, urls );
}

void ReversiMainWindow::readProperties( const KConfigGroup& configGroup )
{
//     const QStringList urls = configGroup.readPathEntry( LoadedUrlsKey, QStringList() );

//     DocumentSyncManager* syncManager = mDocumentManager->syncManager();
//     DocumentCreateManager* createManager = mDocumentManager->createManager();
//     for( int i=0; i<urls.count(); ++i )
//     {
//         if( urls[i].isEmpty() )
//             createManager->createNew();
//         else
//             syncManager->load( urls[i] );
//         // TODO: set view to offset
//         // if( offset != -1 )
//     }
}

void ReversiMainWindow::onViewsOpened( const QList<AbstractView*>& views )
{
    if( ! views.isEmpty() && view() == 0 )
    {
        AbstractView* const view = views.at( 0 );
        setView( view );
    }
}

void ReversiMainWindow::onViewsClosing( const QList<AbstractView*>& views )
{
    AbstractView* const oldView = view();

    if( ! views.isEmpty() && oldView != 0 )
    {
        AbstractView* const view = views.at( 0 );
        if( oldView == view )
            setView( 0 );
    }

}


ReversiMainWindow::~ReversiMainWindow() {}

}
